package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// 👈 SignUpInput struct
type SignUpInput struct {
	Name               string    `json:"name" bson:"name" binding:"required"`
	Email              string    `json:"email" bson:"email" binding:"required"`
	Password           string    `json:"password" bson:"password" binding:"required,min=8"`
	PasswordConfirm    string    `json:"passwordConfirm" bson:"passwordConfirm,omitempty" binding:"required"`
	Role               string    `json:"role" bson:"role"`
	Privilege          string    `json:"privilege" bson:"privilege"`
	VerificationCode   string    `json:"verificationCode,omitempty" bson:"verificationCode,omitempty"`
	ResetPasswordToken string    `json:"resetPasswordToken,omitempty" bson:"resetPasswordToken,omitempty"`
	ResetPasswordAt    time.Time `json:"resetPasswordAt,omitempty" bson:"resetPasswordAt,omitempty"`
	Verified           bool      `json:"verified" bson:"verified"`
	CreatedAt          time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt          time.Time `json:"updated_at" bson:"updated_at"`
}

// 👈 SignInInput struct
type SignInInput struct {
	Email    string `json:"email" bson:"email" binding:"required"`
	Password string `json:"password" bson:"password" binding:"required"`
}

// 👈 Privilege struct
type PrivilegeInput struct {
	Privilege string `json:"privilege" bson:"privilege" binding:"required"`
}

// 👈 DBResponse struct
type DBResponse struct {
	ID                 primitive.ObjectID `json:"id" bson:"_id"`
	Name               string             `json:"name" bson:"name"`
	Email              string             `json:"email" bson:"email"`
	Password           string             `json:"password" bson:"password"`
	PasswordConfirm    string             `json:"passwordConfirm,omitempty" bson:"passwordConfirm,omitempty"`
	Role               string             `json:"role" bson:"role"`
	Privilege          string             `json:"privilege" bson:"privilege"`
	VerificationCode   string             `json:"verificationCode,omitempty" bson:"verificationCode"`
	ResetPasswordToken string             `json:"resetPasswordToken,omitempty" bson:"resetPasswordToken,omitempty"`
	ResetPasswordAt    time.Time          `json:"resetPasswordAt,omitempty" bson:"resetPasswordAt,omitempty"`
	Verified           bool               `json:"verified" bson:"verified"`
	CreatedAt          time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt          time.Time          `json:"updated_at" bson:"updated_at"`
}

// 👈 UpdateInput struct
type UpdateInput struct {
	Name               string    `json:"name,omitempty" bson:"name,omitempty"`
	Email              string    `json:"email,omitempty" bson:"email,omitempty"`
	Password           string    `json:"password,omitempty" bson:"password,omitempty"`
	Role               string    `json:"role,omitempty" bson:"role,omitempty"`
	Privilege          string    `json:"privilege,omitempty" bson:"privilege,omitempty"`
	VerificationCode   string    `json:"verificationCode,omitempty" bson:"verificationCode,omitempty"`
	ResetPasswordToken string    `json:"resetPasswordToken,omitempty" bson:"resetPasswordToken,omitempty"`
	ResetPasswordAt    time.Time `json:"resetPasswordAt,omitempty" bson:"resetPasswordAt,omitempty"`
	Verified           bool      `json:"verified,omitempty" bson:"verified,omitempty"`
	CreatedAt          time.Time `json:"created_at,omitempty" bson:"created_at,omitempty"`
	UpdatedAt          time.Time `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
}

// 👈 UserResponse struct
type UserResponse struct {
	ID              primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	Name            string             `json:"name,omitempty" bson:"name,omitempty"`
	Email           string             `json:"email,omitempty" bson:"email,omitempty"`
	Role            string             `json:"role,omitempty" bson:"role,omitempty"`
	Privilege       string             `json:"privilege,omitempty" bson:"privilege,omitempty"`
	PasswordConfirm string             `json:"passwordConfirm" bson:"passwordConfirm"`
	CreatedAt       time.Time          `json:"created_at" bson:"created_at"`
	UpdatedAt       time.Time          `json:"updated_at" bson:"updated_at"`
}

// 👈 ForgotPasswordInput struct
type ForgotPasswordInput struct {
	Email string `json:"email" bson:"email" binding:"required"`
}

// 👈 ResetPasswordInput struct
type ResetPasswordInput struct {
	Password        string `json:"password" bson:"password"`
	PasswordConfirm string `json:"passwordConfirm,omitempty" bson:"passwordConfirm,omitempty"`
}

// 👈 CreateProfile struct
type CreateProfile struct {
	User      string    `json:"user" bson:"user" binding:"required"`
	Citizen   string    `json:"citizen,omitempty" bson:"citizen,omitempty"`
	Nin       string    `json:"nin,omitempty" bson:"nin,omitempty"`
	Gender    Gender    `json:"gender,omitempty" bson:"gender,omitempty"`
	Education string    `json:"education,omitempty" bson:"education,omitempty"`
	Contact   string    `json:"contact,omitempty" bson:"contact,omitempty"`
	Email     string    `json:"email,omitempty" bson:"email,omitempty"`
	Photo     string    `json:"photo,omitempty" bson:"photo,omitempty"`
	CreatedAt time.Time `json:"created_at,omitempty" bson:"created_at,omitempty"`
	UpdatedAt time.Time `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
}

// 👈 DBProfile struct
type DBProfile struct {
	ID        primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	User      string             `json:"user,omitempty" bson:"user,omitempty"`
	Citizen   string             `json:"citizen,omitempty" bson:"citizen,omitempty"`
	Nin       string             `json:"nin,omitempty" bson:"nin,omitempty"`
	Gender    Gender             `json:"gender,omitempty" bson:"gender,omitempty"`
	Education string             `json:"education,omitempty" bson:"education,omitempty"`
	Contact   string             `json:"contact,omitempty" bson:"contact,omitempty"`
	Email     string             `json:"email,omitempty" bson:"email,omitempty"`
	Photo     string             `json:"photo" bson:"photo"`
	CreatedAt time.Time          `json:"created_at,omitempty" bson:"created_at,omitempty"`
	UpdatedAt time.Time          `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
}

type UpdateProfile struct {
	Citizen   string    `json:"citizen,omitempty" bson:"citizen,omitempty"`
	Nin       string    `json:"nin,omitempty" bson:"nin,omitempty"`
	Gender    Gender    `json:"gender,omitempty" bson:"gender,omitempty"`
	Education string    `json:"education,omitempty" bson:"education,omitempty"`
	Contact   string    `json:"contact,omitempty" bson:"contact,omitempty"`
	Email     string    `json:"email,omitempty" bson:"email,omitempty"`
	Photo     string    `json:"photo,omitempty" bson:"photo,omitempty"`
	CreatedAt time.Time `json:"created_at,omitempty" bson:"created_at,omitempty"`
	UpdatedAt time.Time `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
}

func FilteredResponse(user *DBResponse) UserResponse {
	return UserResponse{
		ID:              user.ID,
		Email:           user.Email,
		Name:            user.Name,
		Role:            user.Role,
		Privilege:       user.Privilege,
		CreatedAt:       user.CreatedAt,
		UpdatedAt:       user.UpdatedAt,
		PasswordConfirm: user.PasswordConfirm,
	}
}
