package models

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

// 👈 CreateMenu struct
type CreateMenu struct {
	ParentId    string    `json:"parentid,omitempty" bson:"parentid,omitempty"`
	MenuType    MenuType  `json:"menuType,omitempty" bson:"menuType,omitempty"`
	Caption     string    `json:"nin,omitempty" bson:"nin,omitempty"`
	Title       string    `json:"gender,omitempty" bson:"gender,omitempty"`
	Controller  string    `json:"education,omitempty" bson:"education,omitempty"`
	Jcode       string    `json:"contact,omitempty" bson:"contact,omitempty"`
	Ordinal     string    `json:"email,omitempty" bson:"email,omitempty"`
	Icon        string    `json:"photo,omitempty" bson:"photo,omitempty"`
	IconType    string    `json:"icontype,omitempty" bson:"icontype,omitempty"`
	Description string    `json:"description,omitempty" bson:"description,omitempty"`
	Isactive    bool      `json:"isactive,omitempty" bson:"isactive,omitempty"`
	CreatedAt   time.Time `json:"created_at,omitempty" bson:"created_at,omitempty"`
	UpdatedAt   time.Time `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
}

// 👈 DBResponse struct
type DBMenu struct {
	ID          primitive.ObjectID `json:"id,omitempty" bson:"_id,omitempty"`
	ParentId    string             `json:"parentid,omitempty" bson:"parentid,omitempty"`
	MenuType    MenuType           `json:"menuType,omitempty" bson:"menuType,omitempty"`
	Caption     string             `json:"nin,omitempty" bson:"nin,omitempty"`
	Title       string             `json:"gender,omitempty" bson:"gender,omitempty"`
	Controller  string             `json:"education,omitempty" bson:"education,omitempty"`
	Jcode       string             `json:"contact,omitempty" bson:"contact,omitempty"`
	Ordinal     string             `json:"email,omitempty" bson:"email,omitempty"`
	Icon        string             `json:"photo,omitempty" bson:"photo,omitempty"`
	IconType    string             `json:"icontype,omitempty" bson:"icontype,omitempty"`
	Description string             `json:"description,omitempty" bson:"description,omitempty"`
	Isactive    bool               `json:"isactive,omitempty" bson:"isactive,omitempty"`
	CreatedAt   time.Time          `json:"created_at,omitempty" bson:"created_at,omitempty"`
	UpdatedAt   time.Time          `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
}

type UpdateMenu struct {
	ParentId    string    `json:"parentid,omitempty" bson:"parentid,omitempty"`
	MenuType    MenuType  `json:"menuType,omitempty" bson:"menuType,omitempty"`
	Caption     string    `json:"nin,omitempty" bson:"nin,omitempty"`
	Title       string    `json:"gender,omitempty" bson:"gender,omitempty"`
	Controller  string    `json:"education,omitempty" bson:"education,omitempty"`
	Jcode       string    `json:"contact,omitempty" bson:"contact,omitempty"`
	Ordinal     string    `json:"email,omitempty" bson:"email,omitempty"`
	Icon        string    `json:"photo,omitempty" bson:"photo,omitempty"`
	IconType    string    `json:"icontype,omitempty" bson:"icontype,omitempty"`
	Description string    `json:"description,omitempty" bson:"description,omitempty"`
	Isactive    bool      `json:"isactive,omitempty" bson:"isactive,omitempty"`
	CreatedAt   time.Time `json:"created_at,omitempty" bson:"created_at,omitempty"`
	UpdatedAt   time.Time `json:"updated_at,omitempty" bson:"updated_at,omitempty"`
}
