package models

// 👈 Enum Value
type MenuType string

const (
	BACKEND  MenuType = "BACKEND"
	FRONTEND MenuType = "FRONTEND"
	LANDING  MenuType = "LANDING"
)

// 👈 Enum Value
type Gender string

const (
	MAN   Gender   = "LAKI-LAKI"
	WOMAN MenuType = "WOMAN"
	NON   MenuType = "PREFER NOT TO SAY"
)
